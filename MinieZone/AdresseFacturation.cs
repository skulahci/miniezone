﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZone
{
    public class AdresseFacturation : Adresse
    {
        public string nomFacturation { get; set; }
        public string prenomFacturation { get; set; }
    }
}
