﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZone
{
    public class Adresse
    {
        public int numeroAd { get; set; }
        public string rueAd { get; set; }
        public int codePostalAd { get; set; }
        public string villeAd { get; set; }
        public string paysAd { get; set; }
    }

}
