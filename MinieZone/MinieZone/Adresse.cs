﻿using System;
using System.Collections.Generic;
using System.Text;
using static MinieZone.ZoneLivraison;

namespace MinieZone
{
    public class Adresse
    {
        public int numeroAd { get; set; }
        public string rueAd { get; set; }
        public int codePostalAd { get; set; }
        public string villeAd { get; set; }
        public string paysAd { get; set; }
        public DateTime dateLiv { get; set; } = (DateTime.Now).AddDays(7);
        public zoneLivraison zoneLivraison { get; set; }

        public int prixLiv()
        {
            switch (zoneLivraison)
            {
                case zoneLivraison.FR:
                    return 5;
                case zoneLivraison.EU:
                    return 10;
                default:
                    return 20;
            }
        }


    }

}
