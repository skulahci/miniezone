﻿using System;
using System.Collections.Generic;
using System.Text;
using static MinieZone.Personne;

namespace MinieZone
{
    public class Commande
    {
        public Guid idCommande { get; set; } = Guid.NewGuid();
        public DateTime dateCommande { get; set; } = DateTime.Now;
        public List<detailCommande> tabDetailCommandes;
        public decimal mttTotalCommande { get; set; }
        public Adresse adresseLiv { get; set; }
        public AdresseFacturation adresseFact { get; set; }
        public Personne.sexe sexeP;


        // Ajoute un montant au compte
        public void mttCommande(List<detailCommande> detailCommandes)
        {
            decimal mttTotalCommande = 0;
            foreach (detailCommande i in detailCommandes)
            {
                mttTotalCommande = i.prixTTC(i.prixHT, i.tauxTaxe);
            }
        }

        public int mttLivraison(List<detailCommande> detailCommandes, Adresse adresse)
        {
            int nbLig = detailCommandes.Count;
            /*
             *Au-dessus de 3 articles la livraison est offerte en France, 
             * au-dessus de 50€ la livraison est offerte au sein de l'UE 
             * et au-dessus de 5 articles qui coutent un minimum de 100€ la livraison 
             * est offerte à l'international.
             */
            if (adresse.zoneLivraison == ZoneLivraison.zoneLivraison.EU)
            {
                if (mttTotalCommande < 50 || nbLig<4 )
                { 
                    return adresse.prixLiv();
                }
            }
            else { 
                if (adresse.zoneLivraison == ZoneLivraison.zoneLivraison.FR)
                {
                    if (nbLig <= 3)
                    {
                        return adresse.prixLiv();
                    }
                }
                else //international
                {
                        if (nbLig<=5 && mttTotalCommande<100)
                        {
                            return adresse.prixLiv();
                        }
                }
            }
            return 0;
        }

        public decimal prixMoyen(List<detailCommande> detailCommandes)
        {
            int nbLig = detailCommandes.Count;
            try
            { 

                return mttTotalCommande / nbLig;
            }
            catch (DivideByZeroException e)
            {
                
                Console.WriteLine("Exception caught: {0}", e);
            }
            return 0;
        }
    }

}

