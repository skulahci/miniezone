﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZone
{
    public class detailCommande : System.Attribute
    {
        public string nomArticle { get; set; }
        public decimal prixHT { get; set; }
        public decimal tauxTaxe
        {
            get { return tauxTaxe; }
            set { tauxTaxe = 20; }
        }

        public decimal prixTTC(decimal xprixHT, decimal xtauxTaxe)
        {
            if (xprixHT < 0)
            {
                return 0;
            }
            return ((xprixHT * xtauxTaxe) / 100);
        }

    }
}
