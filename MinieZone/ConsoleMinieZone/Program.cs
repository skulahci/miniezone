﻿using System;
using MinieZone;
using System.Globalization;

namespace ConsoleMinieZone
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello World!");
            // Create an array of all supported standard date and time format specifiers.
            string[] formats = {"d", "D", "f", "F", "g", "G", "m", "o", "r",
                          "s", "t", "T", "u", "U", "Y"};
            // Create an array of four cultures.                                 
            CultureInfo[] cultures = {CultureInfo.CreateSpecificCulture("en-US"),
                                CultureInfo.CreateSpecificCulture("pl-PL"),
                                CultureInfo.CreateSpecificCulture("fr-FR")};
            // Define date to be displayed.
            DateTime dateToDisplay = DateTime.Now; //new DateTime(2008, 10, 1, 17, 4, 32);

            // Iterate each standard format specifier.
            foreach (string formatSpecifier in formats)
            {
                foreach (CultureInfo culture in cultures)
                    Console.WriteLine("{0} Format Specifier {1, 10} Culture {2, 40}",
                                      formatSpecifier, culture.Name,
                                      dateToDisplay.ToString(formatSpecifier, culture));
                Console.WriteLine();
            }

        }
       
    }
}
