﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZone
{
    public class detailCommande
    {
        public string nomArticle { get; set; }
        public int prixHT { get; set; }
        public double tauxTaxe
        {
            get { return tauxTaxe; }
            set { tauxTaxe = 20; }
        }

        public double prixTTC(double xprixHT, double xtauxTaxe)
        {
            return ((xprixHT * xtauxTaxe) / 100);
        }
    }
}
